import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-resource',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private router: Router,
              private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.fb.group({
      username: '',
      password: ''
    });
  }

  login() {
    const formData = this.loginForm.value;
    if (formData.username === 'demo' && formData.password === 'demo') {
      this.router.navigateByUrl('/resource');
    }
  }

}
