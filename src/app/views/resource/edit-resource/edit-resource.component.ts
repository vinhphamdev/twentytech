import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceService } from '../resource.service';
import { pluck, switchMap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-resource',
  templateUrl: './edit-resource.component.html',
  styleUrls: ['./edit-resource.component.scss']
})
export class EditResourceComponent implements OnInit {
  editForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private resource: ResourceService,
              private route: ActivatedRoute,
              private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getResourceById();
  }

  getResourceById() {
    this.route.params.pipe(
      pluck('id'),
      switchMap(id => this.resource.getResourceById(id))
    ).subscribe(resource => {
      this.editForm.patchValue(resource);
    });
  }

  buildForm() {
    this.editForm = this.fb.group({
      id: '',
      title: '',
      body: '',
      userId: ''
    });
  }

  update() {
    const formData = this.editForm.value;
    this.resource.updateResource(formData.id, formData).subscribe(_ => {
      this.toastr.success('Cập nhật thành công', 'Thông báo');
      this.router.navigate(['../../'], {relativeTo: this.route});
    });
  }

  back() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }
}
