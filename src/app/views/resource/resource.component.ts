import { Component, OnInit } from '@angular/core';
import { ResourceService } from './resource.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {
  resources = [];

  constructor(private resource: ResourceService,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getListResource();
  }

  getListResource() {
    this.resource.getListResource().subscribe(res => {
      this.resources = res as any;
    });
  }

  create() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  delete(id) {
    this.resource.deleteResource(id).subscribe(_ => {
      this.toastr.success('Xóa thành công', 'Thông báo');
      this.getListResource();
    });
  }
}
