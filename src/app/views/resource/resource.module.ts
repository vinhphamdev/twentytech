import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { CommonModule } from '@angular/common';
import { ResourceRoutingModule } from './resource-routing.module';
import { ResourceComponent } from './resource.component';
import { CreateResourceComponent } from './create-resource/create-resource.component';
import { EditResourceComponent } from './edit-resource/edit-resource.component';

@NgModule({
  imports: [
    FormsModule,
    ResourceRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [ ResourceComponent, CreateResourceComponent, EditResourceComponent ]
})
export class ResourceModule { }
