import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ResourceService {
  private baseUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private router: Router,
              private http: HttpClient
  ) {}

  getListResource() {
    return this.http.get(this.baseUrl);
  }

  createResource(formData) {
    return this.http.post(this.baseUrl, formData);
  }

  getResourceById(id) {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updateResource(id, formData) {
    return this.http.put(`${this.baseUrl}/${id}`, formData);
  }

  deleteResource(id) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}


