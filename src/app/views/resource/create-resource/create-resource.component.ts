import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceService } from '../resource.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-resource',
  templateUrl: './create-resource.component.html',
  styleUrls: ['./create-resource.component.scss']
})
export class CreateResourceComponent implements OnInit {
  createForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private resource: ResourceService,
              private route: ActivatedRoute,
              private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.createForm = this.fb.group({
      title: '',
      body: '',
      userId: ''
    });
  }

  add() {
    const formData = this.createForm.value;
    this.resource.createResource(formData).subscribe(_ => {
      this.toastr.success('Thêm mới thành công', 'Thông báo');
      this.router.navigate(['../'], {relativeTo: this.route});
    });
  }

  back() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
